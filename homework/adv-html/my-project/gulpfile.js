let gulp = require('gulp');
let sass = require('gulp-sass');


gulp.task('sass', function(){
    return gulp.src('./src/sass/**/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('public/css'))
});

gulp.task('watch', function () {
    gulp.watch('.src/sass/**/*.sass', gulp.series('sass'));
});