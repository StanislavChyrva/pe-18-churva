// Написать реализацию кнопки "Показать пароль". Задача должна быть реализована на языке javascript, без использования
// фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     В файле index.html лежит разметка для двух полей ввода пароля.
//     По нажатию на иконку рядом с конкретным полем - должны отображаться символы, которые ввел пользователь,
//     иконка меняет свой внешний вид.
//     Когда пароля не видно - иконка поля должна выглядеть, как та, что в первом поле (Ввести пароль)
// Когда нажата иконка, она должна выглядеть, как та, что во втором поле (Ввести пароль)
// По нажатию на кнопку Подтвердить, нужно сравнить введенные значения в полях
// Если значения совпадают - вывести модальное окно (можно alert) с текстом - You are welcome;
// Если значение не совпадают - вывести под вторым полем текст красного цвета  Нужно ввести одинаковые значения
//
// После нажатия на кнопку страница не должна перезагружаться
// Можно менять разметку, добавлять атрибуты, теги, id, классы и так далее.



const passwordInputCollection = document.getElementsByClassName('password');
for (let element of passwordInputCollection) {
    element.addEventListener("click", () => {
        document.getElementsByClassName('errorSpan')[0].remove();
        document.getElementById('btn').dataset.clickIdentificator = '0';
    })
}

const passwordIcoCollection = document.getElementsByClassName('icon-password');
for (let element of passwordIcoCollection) {
    element.addEventListener("click", showPasswordListener)
}

/**
 * @description Show or hide password and change ico
 */
function showPasswordListener() {
    if (this.className === 'fas fa-eye icon-password') {
        document.getElementsByClassName(this.dataset.icoNumber)[0].type = 'text';
        this.className = 'fas fa-eye-slash icon-password';
    } else if (this.className === 'fas fa-eye-slash icon-password') {
        document.getElementsByClassName(this.dataset.icoNumber)[0].type = 'password';
        this.className = 'fas fa-eye icon-password';
    }
}

/**
 * @description Check password and confirm password
 */
const checkPasswordListener = function () {
   const button = document.getElementById('btn');
    let passwordCollection;
    if (button.dataset.clickIdentificator === '0') {
        passwordCollection = document.getElementsByClassName('password');
    if (passwordCollection[0].value !== '' || passwordCollection[1].value !== '') {
        if (passwordCollection[0].value === passwordCollection[1].value) {
            let ifConfirmPass = document.getElementById('youAreWelcome');
            ifConfirmPass.style.display = 'flex';
            setTimeout(() => {ifConfirmPass.style.display = 'none';}, 2000);
            button.dataset.clickIdentificator = '0';

        } else {
            let errorSpan = document.createElement('span');
            errorSpan.innerHTML = 'Need to enter the same values';
            errorSpan.className = 'errorSpan';
            this.before(errorSpan);
            button.dataset.clickIdentificator = '1';
        }

    }}
};


const confirmButton = document.getElementById('btn');
confirmButton.addEventListener('click', checkPasswordListener);

