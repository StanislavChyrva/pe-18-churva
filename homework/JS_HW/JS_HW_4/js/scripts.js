let userFirstName = prompt('Введите Ваше имя', '');
let userLastName = prompt('Введите Вашу фамилию', '');

/**
 * @description Create new object - new user
 * @param firstName {string}
 * @param lastName {string}
 * @returns {string|{getLogin(): string, firstName: *, lastName: *, setLastName(string): void, setFirstName(string): void}}
 */
function createNewUser(firstName, lastName) {

    let newUserObject = {
        firstName: firstName,
        lastName: lastName,

        /**
         * @description Return string first letter of first name + last name in lower case
         * @returns {string}
         */
        getLogin() {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase()
        },

        /**
         * Change first name in object
         * @param newFirstName {string}
         */
        setFirstName(newFirstName) {
            Object.defineProperty(this, 'firstName', {
                writable: true
            });
            this.firstName = newFirstName;
            Object.defineProperty(this, 'firstName', {
                writable: false
            });
        },

        /**
         * @description Change last name in object
         * @param newLastName {string}
         */
        setLastName(newLastName) {
            Object.defineProperty(this, 'lastName', {
                writable: true
            });
            this.lastName = newLastName;
            Object.defineProperty(this, 'lastName', {
                writable: false
            });
        }
    };

    // Prohibit rewriting  of first name and last name
    Object.defineProperty(newUserObject, 'firstName', {
        writable: false
    });

    Object.defineProperty(newUserObject, 'lastName', {
        writable: false
    });

    return newUserObject;

}

let newUser = createNewUser(userFirstName, userLastName);

console.log(newUser);
console.log(`Login --> ${newUser.getLogin()}`);
