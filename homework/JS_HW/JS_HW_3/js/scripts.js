let userFirstNumber;
let userSecondNumber;
let userMathOperation;


do {
    userFirstNumber = prompt('Введите первое число', userFirstNumber);
    if (userFirstNumber === null) {
        break;
    }
} while (isNaN(userFirstNumber) || userFirstNumber === '');

do {
    userSecondNumber = prompt('Введите второе число', userSecondNumber);
    if (userSecondNumber === null) {
        break;
    }
} while (isNaN(userSecondNumber) || userSecondNumber === '');

do {
    userMathOperation = prompt('Введите математическую операцию (+ - * /)', userMathOperation);
    if (userMathOperation === null) {
        break;
    }
} while (!(userMathOperation === '+' || userMathOperation === '-' || userMathOperation === '*' || userMathOperation === '/'));


/**
 * @param firstNumber
 * @param secondNumber
 * @param mathOperation
 * @returns {number|*}
 */

function mathOperation(firstNumber, secondNumber, mathOperation) { // Функция принимает два числа и математический оператор
    switch (mathOperation) {
        case '+':
            return parseFloat(firstNumber) + parseFloat(secondNumber);
            break;
        case '-':
            return firstNumber - secondNumber;
            break;
        case '*':
            return firstNumber * secondNumber;
            break;
        case '/':
            return firstNumber / secondNumber;
            break;
    }

    // Вариант через if
    // if (mathOperation === '+') {
    //     return firstNumber + secondNumber;
    // } else if (mathOperation === '-') {
    //     return firstNumber - secondNumber;
    // } else if (mathOperation === '*') {
    //     return firstNumber * secondNumber;
    // } else if (mathOperation === '/') {
    //     return firstNumber / secondNumber;
    // }

}

console.log(mathOperation(userFirstNumber, userSecondNumber, userMathOperation));

