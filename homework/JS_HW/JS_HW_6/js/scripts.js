/**
 * @description Function return new array without elements whose type was specified in dataType
 * @param array {array}
 * @param dataType {string}
 * @returns {array}
 */
function filterBy(array, dataType) {
    return array.filter(element => typeof (element) !== dataType)
}

const arrayOfType = ['number', 'string', 'boolean', 'undefined', 'object'];
const arrayToFilter = [1, 2, 4, 'I', 'Am','Array', null, {}, {}, true, true, false, undefined, undefined];

console.log(`Now we start to filter this array:`);
console.log(arrayToFilter);

arrayOfType.forEach(item => {
    console.log(`This is array without ${item}:`);
    console.log(filterBy(arrayToFilter, item));
});

