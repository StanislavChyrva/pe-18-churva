const array1 = ['hello', 'world', 'Kiev', 'Kharkiv', ['one', 'two', ['one more', 'and one more'], 'three'], 'Odessa', 'Lviv'];


/**
 * @description convert array in HTML string to document.write in format <ol> list, work with nested arrays
 * @param array {array}
 * @returns {string}
 */
const convertArrayToHTMLString = function (array) {
    let newArray = array.map(item => {
        if (Array.isArray(item)) {
            return convertArrayToHTMLString(item);
        } else {
            return `<li>${item}</li>`;
        }
    });
    return `<ol>${newArray.join('')}</ol>`;
};


/**
 * @description creates in HTML container with list with array elements
 * @param array {array}
 */
const printArrayAsList = function (array) {
    let div = document.createElement('div');
    div.setAttribute('class', 'arrayListContainer');
    document.body.append(div);
    div.innerHTML = convertArrayToHTMLString(array);
};

/**
 * @description clear all HTML elements with specific class
 * @param className {string}
 */
const clearElementsWithClass = function (className) {
    const allElements = document.getElementsByClassName(className);
    for (let element of allElements) {
        element.setAttribute('style', 'display: none');
    }
};

/**
 * @description create HTML <div> container with countdown timer
 * @param timeInSeconds {number}
 */
const countdownTimer = function (timeInSeconds) {
    const timerElement = document.createElement('div');
    timerElement.setAttribute('class', 'countdownTimer');
    document.body.append(timerElement);
    timerElement.innerHTML = `${timeInSeconds} seconds left`;

    let timerValue = timeInSeconds - 1;
    const timer = function () {
        timerElement.innerHTML = `${timerValue} seconds left`;
        timerValue--;
    };

    let timerId = setInterval(timer, 1000);

    setTimeout(() => {
        clearInterval(timerId);
        timerElement.setAttribute('style', 'display: none');
    }, timeInSeconds * 1000);

};

/**
 * @description clear all HTML element with class = element class after a given time
 * @param elementClass {string}
 * @param timeInSeconds {number}
 */
const setTimerToClearElement = function (elementClass, timeInSeconds) {
    setTimeout(() => clearElementsWithClass(elementClass), timeInSeconds * 1000);
    countdownTimer(timeInSeconds);
};



printArrayAsList(array1);
setTimerToClearElement('arrayListContainer',10);




