/**
 * @description Create new student {object}, prompt student name, last name and array of marks
 * @returns {{lastName: *, averageMark(): *, name: *, marks: *, getBadMarks(): *}|number}
 */
function getNewStudent() {
    let userName = prompt('Enter name of the student', '');
    let userLastName = prompt('Enter last name of the student', '');
    let newMark;
    let marks = [];
    let i = 0;
    do {
        newMark = prompt('Enter mark of the student', '');
        marks[i] = newMark;
        i++;
    } while (newMark !== null);

    marks.length = marks.length - 1;

    return {
        name: userName,
        lastName: userLastName,
        marks: marks,
        /**
         * @description return count of bad marks ( < 4 ). If count = 0 --> alert message
         * @returns {number}
         */
        getBadMarks() {
            let badMarksCount = 0;
            this.marks.forEach(item => {
                if (item < 4) badMarksCount++;
            });
            if (badMarksCount === 0) alert('Студент переведен на следующий курс');
            return badMarksCount;
        },
        /**
         * @description return average mark, if > 7 alert message
         * @returns {number}
         */
        averageMark() {
            let averageMark;
            let sumMark = 0;
            this.marks.forEach(item => sumMark += +item);
            averageMark = sumMark/+this.marks.length;
            if (averageMark > 7) console.log('Студенту назначена стипендия');
            return averageMark;
        }


    };

}

const student = getNewStudent();
