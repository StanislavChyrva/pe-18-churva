// Написать реализацию игры "Сапер". Задача должна быть реализована на языке javascript, без использования фреймворков и сторонник библиотек (типа Jquery).
//
// Технические требования:
//
//     Нарисовать на экране поле 8*8 (можно использовать таблицу или набор блоков).
// Сгенерировать на поле случайным образом 10 мин. Пользователь не видит где они находятся.
//     Клик левой кнопкой по ячейке поля "открывает" ее содержимое пользователю.
//
//     Если в данной ячейке находится мина, игрок проиграл. В таком случае показать все остальные мины на поле. Другие действия стают недоступны, можно только начать новую игру.
//     Если мины нет, показать цифру - сколько мин находится рядом с этой ячейкой.
//     Если ячейка пустая (рядом с ней нет ни одной мины) - необходимо открыть все соседние ячейки с цифрами.
//
//
//     Клик правой кнопки мыши устанавливает или снимает с "закрытой" ячейки флажок мины.
//     После первого хода над полем должна появляться кнопка "Начать игру заново",  которая будет обнулять предыдущий результат прохождения и заново инициализировать поле.
//     Над полем должно показываться количество расставленных флажков, и общее количество мин (например 7 / 10).
//
//
// Необязательное задание продвинутой сложности:
//
//     При двойном клике на ячейку с цифрой - если вокруг нее установлено такое же количество флажков, что указано на цифре этой ячейки, открывать все соседние ячейки.
//     Добавить возможность пользователю самостоятельно указывать размер поля. Количество мин на поле можно считать по формуле Количество мин = количество ячеек / 6.


let mainMineCounter;
let mainMineNumber;

const createField = function (numberOfCells) {
    const gameContainer = document.getElementsByClassName('game__container')[0];
    let cellsCounter = 0;
    for (let i = 0; i < numberOfCells; i++) {
        let fieldsRow = document.createElement('div');
        fieldsRow.setAttribute('class', 'fields__row');
        for (let j = 0; j < numberOfCells; j++) {
            let fieldsCell = document.createElement('div');
            fieldsCell.setAttribute('class', 'fields__cell');
            fieldsCell.setAttribute('data-number-of-cell', `${cellsCounter}`);
            fieldsCell.setAttribute('data-mine', 'false');
            fieldsCell.setAttribute('data-near-mine-checked', 'false');
            fieldsCell.setAttribute('data-row', `${i}`);
            fieldsCell.setAttribute('data-cell', `${j}`);
            fieldsCell.setAttribute('data-flag', `false`);
            cellsCounter++;
            fieldsRow.append(fieldsCell);
        }
        gameContainer.append(fieldsRow);
    }

};


const randomIndexArray = function (numberOfMines) {
    let arrayOfIndexes = [];
    while (arrayOfIndexes.length < numberOfMines) {
        let randomIndex = Math.floor(Math.random() * document.getElementsByClassName('fields__cell').length);
        if (arrayOfIndexes.indexOf(randomIndex) === -1) {
            arrayOfIndexes.push(randomIndex);
        }
    }
    return arrayOfIndexes;
};


const createMines = function (numberOfMines) {
    const arrayOfMines = randomIndexArray(numberOfMines);
    const cellsCollection = document.getElementsByClassName('fields__cell');

    arrayOfMines.forEach(element => {
        for (cell of cellsCollection) {
            let numberOfCell = Number(cell.getAttribute('data-number-of-cell'));
            if (numberOfCell === element) {
                const mine = document.createElement('div');
                mine.setAttribute('class', 'mine');
                mine.classList.add('hidden');
                cell.append(mine);
                cell.dataset.mine = 'true';
            }
        }


    });

};


const nearMineCounter = function () {
    const cellsCollection = document.getElementsByClassName('fields__cell');
    for (let i = 0; i < cellsCollection.length; i++) {
        if (cellsCollection[i].getAttribute('data-near-mine-checked') === 'false') {
            let nearMineCounter = 0;
            let row = Number(cellsCollection[i].getAttribute('data-row'));
            let cell = Number(cellsCollection[i].getAttribute('data-cell'));

            for (elements of cellsCollection) {
                if (Number(elements.dataset.row) === row - 1 || Number(elements.dataset.row) === row || Number(elements.dataset.row) === row + 1) {
                    if (Number(elements.dataset.cell) === cell - 1 || Number(elements.dataset.cell) === cell || Number(elements.dataset.cell) === cell + 1) {
                        if (elements.dataset.mine === 'true') {
                            nearMineCounter++;
                        }
                    }
                }

            }

            cellsCollection[i].setAttribute('data-near-mine-counter', `${nearMineCounter}`);

        }
    }
};


const showNearMineCounter = function (event) {

    if(event.target.dataset.mine === 'true') {
        const minesCollection = document.getElementsByClassName('mine');
        for (mine of minesCollection) {
            mine.classList.remove('hidden');
        }

    } else {

    function internalFunction(cellElement) {
        const cellsCollection = document.getElementsByClassName('fields__cell');
        let row = Number(cellElement.getAttribute('data-row'));
        let cell = Number(cellElement.getAttribute('data-cell'));
        console.log(row);
        console.log(cell);
        console.log();

        if (cellElement.dataset.nearMineCounter !== '0') {
            cellElement.innerHTML = cellElement.dataset.nearMineCounter;
            cellElement.dataset.nearMineChecked = 'true';
            cellElement.classList.add('checked');
        } else {
            for (elements of cellsCollection) {

                if (Number(elements.dataset.row) === row - 1 || Number(elements.dataset.row) === row || Number(elements.dataset.row) === row + 1) {
                    if (Number(elements.dataset.cell) === cell - 1 || Number(elements.dataset.cell) === cell || Number(elements.dataset.cell) === cell + 1) {

                        if (elements.dataset.nearMineChecked === 'false') {
                            if (elements.dataset.nearMineCounter === '0') {
                                elements.classList.add('checked');
                                elements.dataset.nearMineChecked = 'true';
                                internalFunction(elements);
                            } else {
                                elements.innerHTML = elements.dataset.nearMineCounter;
                                elements.dataset.nearMineChecked = 'true';
                                elements.classList.add('checked');

                            }
                        }
                    }

                }
            }
        }
    }

    internalFunction(event.target);
    }
};

const setFlag = function(event) {
    if (event.target.dataset.flag === 'false') {
        event.preventDefault();
        event.target.classList.add('flag');
        event.target.dataset.flag = 'true';
        mainMineCounter--;
        document.getElementsByClassName('mine__counter')[0].innerHTML = `left ${mainMineCounter} mines of ${mainMineNumber}`;
    } else {
        event.preventDefault();
        event.target.classList.remove('flag');
        event.target.dataset.flag = 'false';
        mainMineCounter++;
        document.getElementsByClassName('mine__counter')[0].innerHTML = `left ${mainMineCounter} mines of ${mainMineNumber}`;

    }
};


const startFunction = function (){
 let userNumber = +document.getElementById('numberOfCells').value;
 if (userNumber > 0) {
 document.getElementsByClassName('start__container')[0].classList.add('hidden');
    createField(userNumber);
    createMines(Math.floor((userNumber*userNumber)/6));
    nearMineCounter();

    let gameContainer = document.getElementsByClassName('game__container')[0];
    gameContainer.addEventListener('click', showNearMineCounter);
    gameContainer.addEventListener('contextmenu', setFlag);

    mainMineNumber = Math.floor((userNumber*userNumber)/6);
    mainMineCounter = mainMineNumber;

    let mainMineCounterContainer = document.createElement('div');
    mainMineCounterContainer.setAttribute('class', 'mine__counter');

    mainMineCounterContainer.innerHTML = `left ${mainMineCounter} mines of ${mainMineNumber}`;
    gameContainer.prepend(mainMineCounterContainer);

 }
};



const startButton = document.getElementById('startButton');
startButton.addEventListener('click', startFunction);

