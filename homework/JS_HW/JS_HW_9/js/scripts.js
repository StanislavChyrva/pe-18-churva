// Реализовать переключение вкладок (табы) на чистом Javascript.
//
// Технические требования:
//
// В папке tabs лежит разметка для вкладок. Нужно, чтобы по нажатию на вкладку отображался конкретный текст для нужной
// вкладки. При этом остальной текст должен быть скрыт. В комментариях указано, какой текст должен отображаться для какой
// вкладки.
// Разметку можно менять, добавлять нужные классы, id, атрибуты, теги.
// Нужно предусмотреть, что текст на вкладках может меняться, и что вкладки могут добавляться и удаляться.
// При этом нужно, чтобы функция, написанная в джаваскрипте, из-за таких правок не переставала работать.


/**
 * @description change style: display of all element wits specified class to NONE
 * @param className {string}
 */
const hideAllContent = function (className) {
    const nodeList = document.getElementsByClassName(className);
   for (let i = 0; i < nodeList.length; i++) {
        document.getElementById(nodeList[i].textContent.toLowerCase()).style.display = 'none';
       nodeList[i].classList.remove('active');
    }
};

/**
 * @description change style: display of element with specified ID to BLOCK
 * @param contentId {string}
 */
const showContent = function (contentId) {
    document.getElementById(contentId).style.display = 'block';
};

/**
 *@description Implement tab switching (tabs) in pure Javascript.
 */
const tabsFunction = function() {
    let id = this.textContent.toLowerCase();
    hideAllContent('tabs-title');
    showContent(id);
    this.classList.add('active');
};



const inputs = document.getElementsByClassName("tabs-title");
for (let i = 0; i < inputs.length; i++) {
    inputs[i].addEventListener("click", tabsFunction);
}

