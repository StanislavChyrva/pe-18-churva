/**
 * @desc Return array with id of all buttons
 * @returns {[]}
 */
const getArrayOfKeyCode = function () {
    const buttonsCollection = document.getElementsByTagName('button');
    const arrayOfButtons = [];
    let i = 0;
    for (let element of buttonsCollection) {

        arrayOfButtons[i] = element.id;
        i++;
    }
    return arrayOfButtons;
};

const arrayOfButtons = getArrayOfKeyCode();


document.addEventListener('keydown', (event) => {
    arrayOfButtons.forEach(element => {
        document.getElementById(element).classList.remove('lightblue');
    });
    arrayOfButtons.forEach(element => {
        if (event.code === element) {
            const newElement = document.getElementById(element);

            if (newElement.dataset.onIdentificator === '0') {
                newElement.classList.add('lightblue');
                arrayOfButtons.forEach(element => {
                    document.getElementById(element).dataset.onIdentificator = '0';
                });
                newElement.dataset.onIdentificator = '1';
            } else {
                newElement.classList.remove('lightblue');
                newElement.dataset.onIdentificator = '0';
            }
        } else {
            const newElement = document.getElementById(element);
            newElement.classList.remove('lightblue');
            newElement.dataset.onIdentificator = '0';
        }
    })
});