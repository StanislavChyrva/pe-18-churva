let userFirstName = prompt('Enter your  first name', '');
let userLastName = prompt('Enter your last name', '');
let userBirthDate = prompt('Enter your date of birth in format dd.mm.yyyy', '');

/**
 * @description Create new object - new user
 * @param firstName {string}
 * @param lastName {string}
 * @param birthDate {string}
 * @returns {{getLogin: (function(): string), firstName: *, lastName: *, getAge: (function(): number), getPassword: (function(): string), birthDate: *}}
 */
function createNewUser(firstName, lastName, birthDate) {
    return {
        firstName: userFirstName,
        lastName: userLastName,
        birthDate: userBirthDate,
        /**
         * @description Return User age (full years)
         * @returns {number}
         */
        getAge: function () {
            // SLice user date to year, month, day
            const userYear = this.birthDate.slice(6, 10);
            const userMonth = this.birthDate.slice(3, 5);
            const userDay = this.birthDate.slice(0, 2);
            // create new Date in format of JS
            let reformatUserBirthDate = new Date(`${userYear}-${+userMonth - 1}-${userDay}`);
            //Date now minus date of birth (in milliseconds) divided by the number of milliseconds per year
            return Math.floor((Date.now() - reformatUserBirthDate) / 3.154e+10);
        },
        /**
         * @description Return login - first letter of first name + last name in lower case.
         * @returns {string}
         */
        getLogin: function () {
            let login = this.firstName[0] + this.lastName;
            return login.toLowerCase()
        },
        /**
         * @description Return password - first letter of first name in upper case + last name in lower case + year of birth.
         * @returns {string}
         */
        getPassword: function () {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDate.slice(6, 10);

        }
    };

}

let newUser = createNewUser(userFirstName, userLastName, userBirthDate);
console.log(newUser);
console.log(`First name --> ${newUser.firstName}`);
console.log(`Last name --> ${newUser.lastName}`);
console.log(`Age --> ${newUser.getAge()}`);
console.log(`Login --> ${newUser.getLogin()}`);
console.log(`Password --> ${newUser.getPassword()}`);


