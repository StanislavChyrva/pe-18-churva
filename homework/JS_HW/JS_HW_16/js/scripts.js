let userFirstNumber = parseInt(prompt('Введите число first n', ''));
let userSecondNumber = parseInt(prompt('Введите число second n', ''));
let userFibonacciLength = parseFloat(prompt('Введите length', ''));


function fibonacciNumber(F0, F1, n) { // функция принимает аргументы первого и второго числа, колличества чисел
    let fibonacciNumber = [F0, F1];
    if (n > 0) {
        for (let i = 2; i < n; i++) {
            fibonacciNumber[i] = fibonacciNumber[i - 1] + fibonacciNumber[i - 2]
        }
    } else if (n < 0) {
        for (let i = 2; i < Math.abs(n); i++) {
            fibonacciNumber[i] = fibonacciNumber[i + 2] - fibonacciNumber[i + 1]
        }
    }


    for (let i = 0; i < Math.abs(n); i++) {
        console.log('Fibonacci number ' + i + ' = ' + fibonacciNumber[i])

    }
}

fibonacciNumber(userFirstNumber, userSecondNumber, userFibonacciLength);