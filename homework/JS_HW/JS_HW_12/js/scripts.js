/**
 * @desc determines the order of the displayed image
 * @type {string}
 */
let imageCounter = document.getElementById('imagesWrapper').dataset.imgCounter;


/**
 * @desc Create button to stop or resume slider
 * @type {HTMLButtonElement}
 */
const button = document.createElement('button');
button.innerHTML = 'STOP IT';
button.setAttribute('class', 'button');
button.setAttribute('id', 'button');
button.setAttribute('data-on-identifier', '0');
document.getElementById('slider').append(button);

/**
 * @description change class of element with specified ID
 * @param imgId {string}
 */
const showIMG = function (imgId) {
    const imgToShow = document.getElementById(imgId);
    imgToShow.classList.remove('hide');
    imgToShow.classList.remove('fadeOut');
    imgToShow.classList.add('show');
    imgToShow.classList.add('fadeIn');
    fadeOutTimerId = setTimeout(() => {
        imgToShow.classList.add('fadeOut');
    }, 9500)
};
/**
 * @desc timerId for slider
 */
let slider;

/**
 * @desc timerId for fade out in showIMG()
 */
let fadeOutTimerId;

/**
 * @description create HTML <div> container with countdown timer
 * @param timeInSeconds {number}
 */
const countdownTimer = function (timeInSeconds) {
    const timerElement = document.createElement('div');
    timerElement.setAttribute('class', 'countdownTimer');
    timerElement.setAttribute('id', 'countdownTimer');
    document.getElementById('slider').append(timerElement);
    timerElement.innerHTML = `${timeInSeconds} seconds left`;


    let timerValue = timeInSeconds * 1000;
    const timer = function () {
        if (timerValue >= 1000) {
        timerElement.innerHTML = `${Math.floor(timerValue / 1000)} s ${timerValue.toString().slice(1)} ms left`;
        } else {
                timerElement.innerHTML = `0 s ${timerValue} ms left`;
            }
        timerValue -= 100;
    };

    let timerId = setInterval(timer, 100);

    setTimeout(() => {
        clearInterval(timerId);
        timerElement.remove();
    }, timeInSeconds * 1000);

};

/**
 * @description create slider for 4 img, work with teg <img> and attribute "data-img-counter" in element with ID
 * "imagesWrapper", set interval
 */
function startSlider() {
    countdownTimer(10);

    // const imgCounter = document.getElementById('imagesWrapper');
    const imagesCollection = document.getElementsByTagName('img');
    let imgCounterIndex = Number(imageCounter.slice(3));
    let imgCounterPrefix = 'img';

    for (let images of imagesCollection) {
        images.classList.remove('show');
        images.classList.add('hide');
    }

    showIMG(imageCounter);

    if (imgCounterIndex === 3) {
        imgCounterIndex = 0;
    } else {
        imgCounterIndex++
    }
    imageCounter = `${imgCounterPrefix + imgCounterIndex}`;


}


startSlider();
slider = setInterval(startSlider, 10000);


button.addEventListener('click', () => {

    if (button.dataset.onIdentifier === '0') {
       for (let element of document.getElementsByClassName('countdownTimer')) {
           element.remove();
       }
        clearInterval(slider);
        clearInterval(fadeOutTimerId);
        button.innerText = 'RESUME';
        button.dataset.onIdentifier = '1';

    } else if (button.dataset.onIdentifier === '1') {
        clearInterval(slider);
        if (imageCounter.slice(3) !== '0') {
        imageCounter = `img${Number(imageCounter.slice(3)) - 1}`;
        } else {
            imageCounter = 'img3';
        }
        startSlider();
        slider = setInterval(startSlider, 10000);
        button.innerText = 'STOP IT';
        button.dataset.onIdentifier = '0';
    }
});


