let userNumber;
let numberIdentifier = 0; // Вводим переменную для учета колличества чисел кратных 5

// --------------- ПРОВЕРКА НА ПРАВИЛЬНОСТЬ ВВОДА ----------------------

do { // Пока пользователь не вводит число и пока это число не целое
    userNumber = prompt('Введите ЦЕЛОЕ число', userNumber); // Запрашиваем у пользователя число
    if (userNumber === null) { // Если пользователь нажал отмена то завершаем скрипт
        break;
    }
} while (Number.isInteger(parseFloat(userNumber)) === false);

// ---------------------------------------------------------------------------

for (let i = 1; i <= userNumber; i++) { // Принимаем в решении нашей задачи что 0 не кратен 5, поэтому  начальное значение i = 01
    if (i % 5 === 0) { // Если остаток от деления і на 5  равняется 0
        numberIdentifier++; // увеличиваем переменную колличества кратных чисел
        console.log('Число №' + numberIdentifier + ': ' + i); // Выводим кратное число
    }

}

if (numberIdentifier === 0) { // Если нет кратных чисел
    console.log('Извините, чисел кратных 5 нет'); // Сообщаем об этом
} else {
    console.log('Число пользователя: ' + userNumber);
    console.log('Колличетсво чисел кратных 5: ' + numberIdentifier);
}

//-------------------------------------------------------------------------------
// Необязательное задание продвинутой сложности:

let primeNumberArr = []; // Инициализируем массив в котором будут храниться простые числа

// Функция принимает два значение: end - конечное значение интервала всех чисел для которых ищем; х - непосредственно само число
// Возвращает true  если число поделось без остатка только на 01 и на само себя
function isSimple(end, x) {
    let count = 0; // Инициализируем переменную для подсчета делений без остатка
    for (let i = 1; i < end; i++) {
        if (x % i === 0) {
            count++;
        }
    }
    return count === 2;
}


// Функция забирает значения интервала поиска из input и если число простое то записывает его в массив, потом весь массив
// записывает в textarea в документ.
function primeNumber() {
    let userStartNumber = +document.getElementById('first__number').value;
    let userEndNumber = +document.getElementById('second__number').value;
    for (let i = userStartNumber; i <= userEndNumber; i++) {
        if (isSimple(userEndNumber, i)) {
            primeNumberArr.push(i);
        }
    }
    document.getElementById('prime__number').value = ''; // Обнуляем текст в textarea
    document.getElementById('prime__number').value = primeNumberArr.join(' '); // Записываем новый
    primeNumberArr = []; // Обнуляем массив
}


// Функция для очистки input и textarea
function clearElement() {
    document.getElementById('first__number').value = '';
    document.getElementById('second__number').value = '';
    document.getElementById('prime__number').value = '';
}

