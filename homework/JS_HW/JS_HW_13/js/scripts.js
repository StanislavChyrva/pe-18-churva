const mainClassList = document.getElementById('mainContainer').classList;
const headerClassList =  document.getElementById('headerMainContainer').classList;

const changeThemeButton = document.getElementById('changeThemeButton');
if (localStorage.getItem('themeStatus') !== 'black') {localStorage.setItem('clickCounter', '0');}

const setBlack = function () {
    mainClassList.add('main__container__background__black');
    mainClassList.remove('main__container__background__white');

    headerClassList.add('header__main__container__black');
    headerClassList.remove('header__main__container__white');
};

const setWhite = function () {
    mainClassList.add('main__container__background__white');
    mainClassList.remove('main__container__background__black');

    headerClassList.add('header__main__container__white');
    headerClassList.remove('header__main__container__black');
};

changeThemeButton.addEventListener('click', () => {

    if (localStorage.getItem('themeStatus') === 'white') {
        setBlack();
        localStorage.setItem('themeStatus', 'black');
        localStorage.removeItem('clickCounter');

    } else if (localStorage.getItem('clickCounter') === '0') {
        setBlack();
        localStorage.setItem('themeStatus', 'black');
        localStorage.removeItem('clickCounter');
    } else {
        setWhite();
        localStorage.setItem('themeStatus', 'white');
    }
});

if (localStorage.getItem('themeStatus') === 'black') {
    setBlack();
} else if (localStorage.getItem('themeStatus') === 'white') {
    setWhite()
}


