const input = document.getElementById('priceInput');

/**
 * @description create div container with span with input Value i value > 0 and create div with "Please enter correct price" iv < 0
 * @param inputId {string}
 * @param inputContainerId {string}
 */
const createSpanOfInputValue = function (inputId, inputContainerId) {
    const input = document.getElementById(inputId);
    const inputContainer = document.getElementById(inputContainerId);

    // If input not clear and input value > 0
    if (input.value !== '' && parseFloat(input.value) > 0) {
        let div = document.createElement('div');
        div.setAttribute('id', 'spanContainerId');
        div.setAttribute('class', 'spanPrice');
        inputContainer.prepend(div);
        div.innerHTML = `<div><span id="spanId">Текущая цена: ${input.value}</span><div class="buttonX" id="buttonXId"><i class="fa fa-times-circle-o" aria-hidden="true"></i></div></div>`;
        input.style.backgroundColor = 'lightgreen';
        buttonX();
    }
    if (parseFloat(input.value) < 0) {
        ifNegative();
    }
};

/**
 * @description if click on #buttonXId delete #spanContainerId, and change input
 */
function buttonX() {
    let buttonX = document.getElementById('buttonXId');
    buttonX.addEventListener('click', () => {
        document.getElementById('spanContainerId').remove();
        input.value = '';
        input.style.backgroundColor = 'white';
    });
}

/**
 * @description create div with "Please enter correct price" and change background of input
 */
function ifNegative() {
    input.setAttribute('style', 'border-color: red;');
    let div = document.createElement('div');
    div.setAttribute('id', 'spanErrorId');
    div.setAttribute('class', 'spanPrice');
    div.innerHTML = '<span>Please enter correct price</span>';
    document.getElementById('container').prepend(div);
}


input.onfocus = () => {
    input.setAttribute('style', 'border-color: green');
    let spanPrice = document.querySelector(".spanPrice");
    spanPrice.remove();
};

input.onblur = () => {
    input.setAttribute('style', 'border-color: light-grey');
    createSpanOfInputValue('priceInput', 'container');
};