/**
 * @description animate scroll to internal section of document with some id = this.href of clicked link
 */
const internalNavigation = function () {
    $(`.horizontal__navigation__a`).click(function () {
        let elementClick = $(this).attr("href");
        let destination = $(elementClick).offset().top;
        $(`html`).animate({
            scrollTop: destination
        }, 800);
        return false;

    })
};

/**
 * @description create and add to element button, show it when scroll down more then one window, on click scroll to top.
 */
const addToTopButton = function () {
    const toTopButton = $('<div type="button" class="to__top__button" id="toTopButton"></div>');
    $('body').prepend(toTopButton);


    $(window).on('scroll', function () {
        if ($(window).scrollTop() >= window.innerHeight) {
            $('#toTopButton').fadeIn();
        } else {
            $('#toTopButton').fadeOut();
        }
    });


    $('#toTopButton').on('click', function () {
        $(`html`).animate({
            scrollTop: 0
        }, 800);
    });
};

/**
 * @description hide or show popular post content and change button's text
 */
const slideTogglePopularPost = function () {
    $('#hideContentButton').on('click', function (event) {
        event.preventDefault();
        $('.popular__post__container').slideToggle('slow');
        if ($(this).attr('data-identifier') === '0') {
            $(this).html('SHOW CONTENT');
            $(this).attr('data-identifier', '1');
        } else {
            $(this).html('HIDE CONTENT');
            $(this).attr('data-identifier', '0');
        }
    });
};

$(document).ready(function () {
    internalNavigation();
    addToTopButton();
    slideTogglePopularPost();
});


