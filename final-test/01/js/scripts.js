const str = "профессионал Профессионал — не тот, кто в Профессионал совершенстве профессионал владеет профессионал техникой Профессионал";
const substr = "профессионал";

const getSubstrCount = function (str, substr, index = true) {
    let string;
    let substring;
    if (index === true) {
        string = str;
        substring = substr;
    }

    if (index === false) {
        string = str.toLowerCase();
        substring = substr.toLowerCase();
    }

let mainCounter = 0;
    let j=0;
    let letterCounter = 0;


    for (let i = 0; i <= string.length; i++) {

        if (string[i] === substring[j]) {
            letterCounter++;
            j++;
            if (letterCounter === substring.length) {
                mainCounter++;
                letterCounter = 0;
                j = 0;
            }
        } else {
            letterCounter = 0;
            j = 0;
        }

    }
return mainCounter;
};

console.log(getSubstrCount(str, substr));
console.log(getSubstrCount(str, substr, false));