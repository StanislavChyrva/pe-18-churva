let patient = {
    firstName: "Stansilav",
    lastName: "Chyrva",
    dateOfBirth: "03.01.1990",
    visits: [],
    /**
     * @description обавляет переданную дату в массив визитов, если в переменную date записана дата в правильном формате
     * "дд.мм.гггг" и возвращет true. Если же формат даты не подходит под требования, он ее не добавляет и возвращает
     * false.
     * @param date {string} in format (dd.mm.yyyy)
     * @returns {boolean}
     */
    addVisit: (date) => {
        // SLice user date to year, month, day
        const userYear = Number(date.slice(6, 10));
        const userMonth = Number(date.slice(3, 5));
        const userDay = Number(date.slice(0, 2));
        let nowDate = new Date();

        if (isNaN(userYear) || userYear > nowDate.getFullYear() || userYear.toString().length !== 4 ) {
            return false;
        } else if (isNaN(userMonth) || userMonth < 1 || userMonth > 12) {
            return false;
        } else if (isNaN(userDay) || userDay < 1 || userDay > 31) {
            return false;
        } else {
            patient.visits.push(date);
            return true;
        }
    },
    /**
     * @description возвращает количество дней, прошедших с момента последнего визита.
     * @returns {string}
     */
    getLastVisitDays: () => {
        const lastVisit = patient.visits[patient.visits.length - 1];
        const year = lastVisit.slice(6, 10);
        const month = lastVisit.slice(3, 5);
        const day = lastVisit.slice(0, 2);
        // create new Date in format of JS
        let reformatLastVisitDate = new Date(`${year}-${month}-${day}`);
        //Date now minus date of birth (in milliseconds) divided by the number of milliseconds per day
        return `Last visit was ${Math.floor((Date.now() - reformatLastVisitDate) / 8.64e+7)} days ago`;
    }
};

