const patientVisits = [
    {
        date: "13.07.2019",
        time: "25",
        diagnosis: "Лишний вес"
    },
    {
        date: "25.08.2019",
        time: "45",
        diagnosis: "Лишний вес. Тахикардия"
    },
    {
        date: "03.09.2019",
        time: "15",
        diagnosis: "Лишний вес. Тахикардия."
    },
    {
        date: "26.09.2019",
        time: "35",
        diagnosis: "Лишний вес. Тахикардия. Стенокардия."
    },
    {
        date: "26.01.2020",
        time: "35",
        diagnosis: "Лишний вес. Тахикардия. Стенокардия."
    },
];
/**
 * @description Извлекает из массива объектов patientVisits все даты визитов пациента в отдельный массив visits.
 * @param patientVisitsArray {array}
 * @returns {[]}
 */
const allVisits = function (patientVisitsArray) {
    const allVisitsArray = [];
    let i = 0;
    patientVisitsArray.forEach(element => {
        allVisitsArray[i] = element.date;
        i++;
    });
    return allVisitsArray;
};
/**
 * @description Находит все визиты длительностью дольше 30 мин и сохраняет их в отдельный массив longVisits.
 * @param patientVisitsArray {array}
 * @returns {[]}
 */
const longVisits = function (patientVisitsArray) {
    const longVisitsArray = [];
    let i = 0;
    patientVisitsArray.forEach(element => {
        if (Number(element.time) > 30) {
            longVisitsArray[i] = element.time;
            i++;
        }
    });
    return longVisitsArray;
};

/**
 * @description Считает сумарно сколько времени было потрачено на прием этого пациента за год.
 * @param patientVisitsArray {array}
 * @param year {number} in format of "yyyy"
 * @returns {number}
 */
const allVisitsTimePerYear = function (patientVisitsArray, year) {
    let allVisitsTimePerYear = 0;

    patientVisitsArray.forEach(element => {
        if (Number(element.date.slice(6, 10)) === year) {
            allVisitsTimePerYear += Number(element.time);
        }
    });
    return allVisitsTimePerYear;
};

/**
 * @description Считает сколько визитов этот пациент совершил в прошлом году.
 * @param arrayOfVisits {array}
 * @returns {number}
 */
const lastYearVisitsQuantity = function (arrayOfVisits) {
    let nowDate = new Date();
    const lastYear = nowDate.getFullYear() - 1;
    const arrayOfAllYear = arrayOfVisits.map(item => {
        return `${item.slice(6, 10)}`;
    });
    const arrayOfLastYear = arrayOfAllYear.filter(element => element.toString() === lastYear.toString());

    return arrayOfLastYear.length;


};


allVisitsArray = allVisits(patientVisits);
console.log(`1. - ${allVisitsArray}`);

longVisitsArray = longVisits(patientVisits);
console.log(`2. - ${longVisitsArray}`);

console.log(`3. - Visits time in 2019: ${allVisitsTimePerYear(patientVisits, 2019)}`);
console.log(`3. -Visits time in 2020: ${allVisitsTimePerYear(patientVisits, 2020)}`);

console.log(`4. -Quantity of last year visits = ${lastYearVisitsQuantity(allVisitsArray)}`);


