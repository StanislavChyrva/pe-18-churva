let userFirstNumber = parseFloat(prompt('Введите первое число', ''));
let userSecondNumber = parseFloat(prompt('Введите еще одно', ''));

while (isNaN(userFirstNumber)) {
    userFirstNumber = parseFloat(prompt('Я же просил число!! Введите первое ЧИСЛО', ''));
}

while (isNaN(userSecondNumber)) {
    userSecondNumber = parseFloat(prompt('Я же просил число!! Введите второе ЧИСЛО', ''));
}

function numberSum (a, b) {
    let sum = a + b;
    let div = document.createElement('p');
    div.className = 'sum';
    div.innerHTML = 'Сумма чисел = ' + sum + '! ';
    document.body.append(div);
}

function numberProd (a, b) {
    let prod = a * b;
    let div = document.createElement('p');
    div.className = 'prod';
    div.innerHTML = 'Произведение чисел = ' + prod + '! ';
    document.body.append(div);
}

numberSum (userFirstNumber, userSecondNumber);
numberProd (userFirstNumber, userSecondNumber);
