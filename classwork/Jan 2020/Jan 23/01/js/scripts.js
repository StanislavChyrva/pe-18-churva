function product (x, y, z) {
    if (typeof(x) !== 'number') {
        x = 1;
    }
    if (typeof(y) !== 'number') {
        y = 1;
    }
    if (typeof(z) !== 'number') {
        z = 1;
    }

    return x * y * z;
}
let aArray = [40, 30, 50, 20, 10, 1, 12, 231, 21, 22];

/**
 * @description Sort array
 * @param array {Array}
 * @returns {Array}
 */

function bubbleSort(array) {

    for (let i = 0, endI = array.length - 1 ; i < endI; i++) {
        for (let j = 0, endJ = endI - i; j < endJ; j++) {
        let b;
        if (array[j] > array[j+1]) {
            b = array[j];
            array[j] = array[j+1];
            array[j+1] = b;
        }
    }
    }
    return array;
}

/**
 * @description Get the smallest number
 * @param x {Number}
 * @param y {Number}
 * @param z {Number}
 * @returns {Number}
 */
function getMinNumber (x, y, z) {
    let array = [x, y, z];
    array = bubbleSort(array);
    return array[0];
}

console.log(aArray);
console.log(aArray = bubbleSort(aArray));
console.log(getMinNumber(12123, 321, 30));