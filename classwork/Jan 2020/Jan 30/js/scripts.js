const string = 'hyper text transfer protocol this first letter in upper case';


/**
 * @description Create abbreviation
 * @param string {string}
 * @returns {string}
 */
function abbreviationString(string) {
    let abbreviation = '';
    let splitStringsArray = string.split(' ');

    for (let i = 0; i < splitStringsArray.length; i++) {
        abbreviation +=splitStringsArray[i][0].toUpperCase();
    }

    return abbreviation;
}

console.log(abbreviationString(string));


/**
 * @description Create new string with first letter of all words in upper case
 * @param string {string}
 * @returns {string}
 */
function firstLetterToUpperCase (string) {
    let newArray = [];
    let splitStringsArray = string.split(' ');

    for (let i = 0; i < splitStringsArray.length; i++) {
        newArray[i] = splitStringsArray[i][0].toUpperCase() + splitStringsArray[i].slice(1);
    }

    return newArray.join(' ');
}

console.log(firstLetterToUpperCase(string));


/**
 * @description All second symbol of string in upper case
 * @param string {string}
 * @returns {string}
 */
function allSecondSymbol(string) {
    let newString = '';

    for(let i = 0; i < string.length; i++) {
        if (i % 2 === 0) {
            newString += string[i].toUpperCase();
        } else {
            newString += string[i];
        }
    }

    return newString;
}

console.log(allSecondSymbol(string));


/**
 * @description Reverse string
 * @param string  {string}
 * @returns {string}
 */
function reverseString(string) {
    let newString = '';

    for(let i = string.length - 1; i >=0 ; i--) {
        newString += string[i];
    }
    return newString;
}

console.log(reverseString(string));




const weekDays = ['sunday', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'];

function createNewUser() {
    let userFirstName = prompt('Введите Ваше имя', '');
    let userLastName = prompt('Введите Вашу фамилию', '');
    let userAge = prompt('Введите возраст дд.мм.гггг', '');

    return  {
        firstName: userFirstName,
        lastName: userLastName,
        dateOfBirth: userAge,
        getYearOfBirth: function() {
            let nowDay = new Date();
            return +nowDay.getFullYear() - this.age;
        }
    };

}

let user1 = createNewUser();

console.log(user1);