const JCurrency = (function () {

    let currentCurrency = 'UAH';

    const rateList =
        {
            UAH: {
                USD: 24.50,
                EUR: 27.34
            },
            USD: {
                USD: 0.046,
                EUR: 1.12
            }
        };



    return {
        name: 'jQCurrency',
        content: 'library',
        getCurrentCurrency: () => currentCurrency,
        setCurrentCurrency: (currency) => currentCurrency = currency,
        currentCurrencyToUsd: (quantity = 1) => {
            return rateList[currentCurrency]['USD'] * quantity;

        }
    };
})();