/* ЗАДАНИЕ - 3
* Написать функцию, которая будет суммировать ВСЕ числа, которые будут переданы ей в качестве аргументов.
* */

const getSum = function (...number) {
    let sum = 0;
    for (let i = 0; i < number.length; i++) {
        sum += number[i];
    }
    return sum;
};

/* ЗАДАНИЕ - 4
* Написать СТРЕЛОЧНУЮ функцию, которая выводит переданное ей аргументом сообщение, указанное количество раз.
* Принимает два аргумента - само сообщение и число сколько раз его показать.
* Если первый аргумент(сообщение) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу - "Empty message"
* Если второй аргумент(количество раз) не передан - ПО УМОЛЧАНИЮ присвоить этому аргументу значение 01.
* */


const displayMessage = (message = 'Empty message', howMuch = 1) => {
    for (let i = 1; i <= howMuch; i++) {
        console.log(message);
    }
};

/* ЗАДАНИЕ - 5
* Написать СТРЕЛОЧНУЮ функцию, которая возвращает максимальный переданый ей аргумент.
* Т.е. функции может быть передано потенциально бесконечное количество аргументов(чисел),
* вернуть нужно самый большой из них.
* */

// const maxArgument = (...arguments) => {
//     return Math.max.apply(null, arguments);
// };

const maxArgument = (...arguments) => {
    return Math.max(...arguments)
};

const macArgumentOld = (...arguments) => {
    let maxNumb = Number.NEGATIVE_INFINITY;
    for (let i of arguments) {
        if (i > muxNumb) {
            maxNumb = i;
        }
    }
    return maxNumb;
};


const fruitShop = {
    admin: {
        secretCode: Math.random()*100
    },
    items: {
        banana: 5,
        orange: 1,
        apple: 2,
        pineapple: 0,
        watermelon: 7,
        kiwi: 8,
        lemon: 4,
        melon: 0
    },

    getFruit: function (fruit, amountOfItems) {
        if (fruit in this.items) {
            if (this.items[fruit.toLowerCase()] < amountOfItems) {
                console.error('No such fruits in the shop');

            } else {
                this.items[fruit.toLowerCase()] -= amountOfItems;
            }
        } else {
            console.error('No such fruits in the shop');
        }
    },

    addFruits: function (fruit, amountOfItems) {
        if (fruit in this.items) {
          return this.items[fruit] += amountOfItems;
    }
        this.items[fruit] = amountOfItems;


}
};