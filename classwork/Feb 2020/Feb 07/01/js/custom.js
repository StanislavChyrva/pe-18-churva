/**
 *
 * @type {{getAllElement: (function(): HTMLCollectionOf<Element>)}}
 */
const customDOM = {
    /**
     * @description Get all elements
     * @returns {HTMLCollectionOf<Element>}
     */
    getAllElement: () => document.getElementsByTagName('*'),
    /**
     * @description Create new element
     * @param tagName
     * @returns {object}
     */

    /**
     *
     * @param id
     * @returns {Element}
     */
    getById: (id) => document.getElementById(id),

    /**
     *
     * @param tagName
     * @returns {Element}
     */
    createElement: (tagName) => document.createElement(tagName),

    /**
     * @description add new child-element to parents
     * @param parent {object}
     * @param child {object}
     * @returns {*|ActiveX.IXMLDOMNode}
     */
    append: (parent, child) => parent.appendChild(child)

};