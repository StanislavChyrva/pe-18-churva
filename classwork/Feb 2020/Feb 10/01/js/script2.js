document.addEventListener('DOMContentLoaded', () => {
    const btn = document.getElementById('send');

    if (!btn) {
        console.error('Element with ID "send" is not found');
        return;
    }

    btn.setAttribute('disabled', 'disabled');
    btn.addEventListener('click', onSendButtonClick, );
    btn.removeAttribute('disabled');
});

function onSendButtonClick() {
    const firstName = document.getElementById('firstName');
    const phone = document.getElementById('phone');

    if (!firstName || !phone) {
        console.error('Element with ID "firstName"  or "phone" is not found');
        return;
    }

    const data = {
        firstName: firstName.value,
        phone: phone.value
    }

    console.log(data);

}