function changeColor() {
    document.body.setAttribute('style', `color: rgb(${(Math.random * 255).toFixed()},
    ${(Math.random * 255).toFixed()},${(Math.random * 255).toFixed()});`)
}

function changeColorInterval() {
    setTimeout(() => {
        changeColor();
        changeColorInterval();
    }, 1000)
}

changeColorInterval();


