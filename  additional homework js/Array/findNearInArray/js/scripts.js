// Write a function that will receive an array of numbers and a number. Return true if the array contains
// the number with +/- 1 accuracy. I.e. 3 will be true for range from 2 to 4.
// For example, containsNear([1, 2, 3], 4) will return true.


/**
 * @description return true if the array contains the number with +/- 1 accuracy.
 * @param arr {array}
 * @param val {number}
 * @returns {boolean}
 */
function containsNear(arr, val) {
    return (arr.includes(val) || arr.includes(val + 1) || arr.includes(val - 1))
}

console.log(containsNear([1, 2, 3, 4, 5], 7));