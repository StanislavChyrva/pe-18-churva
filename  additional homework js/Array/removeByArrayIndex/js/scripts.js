// Write a function that will receive two argument - an array and an index. Remove array element at the index
// position (index starts from 0) and return modified array. Array item must be removed, not set to undefined
// , 0 or null. Array length should change.
// For example, removeByIndex([1, 2, 3, 4],  2) should return [1, 2, 4].

/**
 * @description Remove array element at the index position
 * @param arr {array}
 * @param index {number}
 * @returns {array}
 */
function removeByIndex(arr, index) {
    let newArray = arr.splice(index + 1);
    arr.pop();
    return arr.concat(newArray);
}

console.log(removeByIndex([1, 2, 3, 4, 5, 6, 7, 8], 4));
