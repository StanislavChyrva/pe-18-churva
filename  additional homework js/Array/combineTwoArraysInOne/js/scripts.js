// Write a function that will receive two arrays as arguments and return the combined array of provided two.
// For example, combineArrays([1, 2, 3], [4, 5]) should return [1, 2, 3, 4, 5];

const arrA = [1, 2, 3, null, undefined, 1717];
const arrB = [4, 5, 'hello', 'world'];

/**
 * @description return the combined array of provided two
 * @param arrA {array}
 * @param arrB {array}
 * @returns {array}
 */
function combineArrays(arrA, arrB) {
    return arrA.concat(arrB);
}

console.log(combineArrays(arrA, arrB));