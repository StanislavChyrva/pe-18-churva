// Write a function that will receive an array of numbers (including negative numbers and/or zero) and will
// return the largest number in it.
// For example, greatestValue([1, -7, -2, 10, 4]) will return 10.


/**
 * @description return the largest number of array
 * @param arr {array} must includes only numbers
 * @returns {number}
 */
function greatestValue(arr) {
    return arr.sort((a, b) => a - b)[arr.length - 1];
}

console.log(greatestValue([1, 100, -7, -2,  10, 4]));