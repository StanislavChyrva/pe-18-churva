// Given an array that represents a stack of some data with the limited amount of data in it, write a function
// that will add to the end of the array new value and at the same time will remove the first item in an array,
// so that total length of the array will remain the same.
// For example, addToStack([1, 2, 3],  4) should return [2, 3, 4];

let array = [1, 2, 3, 4, 5]

/**
 * @description add to the end of the array new value and at the same time will remove the first item in an array
 * @param arr {array}
 * @param item {*}
 * @returns {array}
 */
function addToStack(arr, item) {
    arr.push(item);
    arr.splice(0, 1);
    return arr;
}

addToStack(array, 6);

console.log(array);