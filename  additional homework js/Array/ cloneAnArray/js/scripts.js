// Write a function that will receive an array.  Return this array clone, ie it should not be a link
// to a provided array, but a whole new one.
// For example,
// var arr = [1,2,3];
// var newArr = cloneArray(arr);
// newArr[0] = 4;
//arr == [1, 2, 3];
//newArr == [4, 2, 3];



/**
 * @description return array clone
 * @param arr {Array}
 * @returns {Array}
 */
function cloneArray(arr) {
    return arr.map((item) => item);
}

let arr = [1,2,3];
let newArr = cloneArray(arr);
newArr[0] = 4;

console.log(arr);
console.log(newArr);


