// Given an array of objects containing field id, write a function that will receive as arguments such array
// and an id value. Using modern Array methods, this function should find one element whose id is identical
// to the provided one as the argument and return the found object.


const arrayOfObjects = [
    {name: 'first', id: 1},
    {name: 'second', id: 2},
    {name: 'third', id: 3},
    {name: 'fours', id: 4},
    {name: 'fifth', id: 5},
    {name: 'sixth', id: 6},
    {name: 'seventh', id: 7},
    {name: 'eighth', id: 8},
    {name: 'ninth', id: 9}
];


/**
 * @description find one element whose id is identical to the provided one as the argument and return it.
 * @param arr {array}
 * @param id {*}
 * @returns {*}
 */
function findUser(arr, id) {
    let user;
    arr.forEach(item => {
        if (item.id === id) {
            user = item;
        }
    });
    return user;
}


/**
 * @description find one element whose id is identical to the provided one as the argument and return it.
 * @param arr {array}
 * @param id {*}
 * @returns {*}
 */
function findUserSecond(arr, id) {
    return arr.find(item => item.id === id);

}

console.log(findUser(arrayOfObjects, 4));
console.log(findUserSecond(arrayOfObjects, 4));