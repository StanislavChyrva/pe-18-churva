// Having an array of objects that have value age, write a function that will sort this array based on the age field (from lowest to highest) and will return the sorted array. You need to sort using compare function.
// For example, given following code:
//     var users = [
//         {name: "Mike", age: 25},
//         {name: "Jhon", age: 20},
//         {name: "Dan", age: 22}
//     ];
// var result = sortByAge(users);
// result will be equal to
//     [
//     {name: "Jhon", age: 20},
//         {name: "Dan", age: 22},
//         {name: "Mike", age: 25}
//     ];


const users = [
    {name: "Mike", age: 25},
    {name: "Jhon", age: 20},
    {name: "Dan", age: 22},
    {name: "Mike", age: 40},
    {name: "Jhon", age: 27},
    {name: "Dan", age: 33}
];


function sortByAge(arr) {
    return arr.sort((item1, item2) => item1.age - item2.age);
}

console.log(sortByAge(users));