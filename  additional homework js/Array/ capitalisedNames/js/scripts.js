// When secretary added new Users to our list of students, capslock was broken, so now all names
// start with a lowercase first letter;
// You need to write a function and return new array with all names inside it starting with the
// first symbol in an upperCase.


/**
 * @description return new array with all names inside it starting with the first symbol in an upperCase.
 * @param arrayList {array}
 * @returns {*|Uint8Array|BigInt64Array|*[]|Float64Array|Int8Array|Float32Array|Int32Array|Uint32Array|Uint8ClampedArray|BigUint64Array|Int16Array|Uint16Array}
 */
function makeFirstSymbolUpperCase(arrayList) {
    return arrayList.map(item => {
        return item[0].toUpperCase() + item.slice(1);
    })
}

let arrayOfNames = ['jon', 'rupert', 'sam', 'ben', 'lora'];

console.log(makeFirstSymbolUpperCase(arrayOfNames));
