const ourAmazingWork = {
    /**
     * @description imageToShowQuantity {number}, using in showContent function, changing by loadMore function
     */
    imageToShowQuantity: 12,
    /**
     * @description selectedTab {string}, using in loadMore function, changing by showContent function
     */
    selectedTab: 'all',
    /**
     * @description return array of random non-repeating numbers, array length - quantity of showing images, range - quantity of all image container;
     * @returns {[]}
     */
    randomIndexArray: function () {
        let arrayOfIndexes = [];
        while (arrayOfIndexes.length < ourAmazingWork.imageToShowQuantity) {
            let randomIndex = Math.floor(Math.random() * document.getElementsByClassName('our__work__content').length);
            if (arrayOfIndexes.indexOf(randomIndex) === -1) {
                arrayOfIndexes.push(randomIndex);
            }
        }
        return arrayOfIndexes;
    },

    /**
     * @description add to main container of Our Amazing Work sections <div> with images with some class on front side and inform card on back side.
     * Image directory must be in format "img/imageClass/imageClass(# from 1 to imageQuantity).jpg"
     * @param imageQuantity {number}
     * @param imageClass {string}
     */
    loadImages: function (imageQuantity, imageClass) {
        //create <span> - text in imgContainerBack (imageClass in upper case)
        let spanText;
        if (imageClass === 'wordpress') {
            spanText = imageClass.toUpperCase();
        } else {
            spanText = `${imageClass.split('-')[0].toUpperCase()} ${imageClass.split('-')[1].toUpperCase()}`;

        }

        const mainContainer = document.getElementsByClassName('our__work__content__container')[0];
        for (let i = 1; i <= imageQuantity; i++) {

            //create main container
            let imgContainer = document.createElement('div');
            imgContainer.setAttribute('class', 'our__work__content');
            imgContainer.classList.add(imageClass);

            //create front container
            let imgContainerFront = document.createElement('div');
            imgContainerFront.setAttribute('class', 'our__work__content__front');
            imgContainerFront.style.backgroundImage = `url("img/${imageClass}/${imageClass}${i}.jpg")`;

            //create back container
            let imgContainerBack = document.createElement('div');
            imgContainerBack.setAttribute('class', 'our__work__content__back');
            imgContainerBack.classList.add('flex__column');
            imgContainerBack.innerHTML =
                `                <div class="our__work__content__ico__wrapper flex__row">\n` +
                `                    <a href=""><div class="our__work__content__link__ico flex__row"><i class="fas fa-link"></i></div></a>\n` +
                `                   <a href=""><div class="our__work__content__search__ico flex__row"><i class="fas fa-search"></i></div></a>\n` +
                `                </div>\n` +
                `                <h3>CREATIVE DESIGN</h3>\n` +
                `                <span>${spanText}</span>\n` +
                `            </div>\n` +
                `        </div>`;

            mainContainer.append(imgContainer);
            imgContainer.append(imgContainerFront);
            imgContainer.append(imgContainerBack);

        }
    },

    /**
     * @description change classList of clicked link and add <div> class="our__work__active__line"(using pure JS)
     */
    tabSwitch: function () {
        const tabsCollection = document.getElementsByClassName('our__work__element__container');
        const line = document.createElement('div');
        line.setAttribute('class', 'our__work__active__line');

        tabsCollection[0].classList.add('our__work__active');
        tabsCollection[0].prepend(line);

        for (let element of tabsCollection) {
            element.onclick = (event) => {
                for (let element of tabsCollection) {
                    element.classList.remove('our__work__active');
                    line.remove();
                }
                event.target.classList.add('our__work__active');
                event.target.prepend(line);
            }
        }
    },
    /**
     * @description show images with class = href of clicked tabs link and hide all other
     * (using jquery)
     */
    showContent: function () {

        const tabs = $('.our__work__element__container');
        let allImages = $(`.our__work__content`);

        //show random content of first tab "all"
        ourAmazingWork.randomIndexArray().forEach((element) => {
            $($(allImages)[element]).fadeIn(1500);
        });

        tabs.on('click', function (event) {
                let allImages = $(`.our__work__content`);
                let imageClass = $(event.target).parent().attr('href').split('#')[1];
                //change global ourAmazingWork.selectedTab
                ourAmazingWork.selectedTab = imageClass;
                let selectedImages = $(`.${imageClass}`);

                //hide all content
                $(allImages).each(function () {
                    $(this)[0].style.display = 'none';
                });

                //show content of clicked tab
                if (imageClass === 'all') {
                    ourAmazingWork.randomIndexArray().forEach((element) => {
                        $($(allImages)[element]).fadeIn(1500);
                    });
                } else {
                    for (let i = 0; i < ourAmazingWork.imageToShowQuantity; i++) {
                        $($(selectedImages)[i]).fadeIn(1500);
                    }
                }
            }
        )
    },

    /**
     * @description load more 12 images and show it
     */
    loadMore: function () {
        let clickCounter = 0;

        $('#loadMore').on('click', function () {
            clickCounter++;

            ourAmazingWork.loadImages(12, 'web-design');
            ourAmazingWork.loadImages(12, 'graphic-design');
            ourAmazingWork.loadImages(12, 'landing-page');
            ourAmazingWork.loadImages(12, 'wordpress');

            ourAmazingWork.imageToShowQuantity += 12;
            //hide button
            $('#loadMore').hide('slow');

            //add preloader
            const preloader = document.createElement('div');
            preloader.setAttribute('class', 'flex__row');
            preloader.innerHTML = '<div class="gooey">\n' +
                '  <span class="dot"></span>\n' +
                '  <div class="dots">\n' +
                '    <span></span>\n' +
                '    <span></span>\n' +
                '    <span></span>\n' +
                '  </div>\n' +
                '</div>';
            this.before(preloader);

            //remove preloader after 2 second timeout
            let preLoaderRemove = setTimeout(function () {
                preloader.remove();
            }, 2000);

            //show new images after 2 second timeout
            let showNewImages = setTimeout(function () {
                if (ourAmazingWork.selectedTab === 'all') {
                    let allImages = $(`.our__work__content`);

                    //hide all images
                    $(allImages).each(function () {
                        $(this)[0].style.display = 'none';
                    });

                    //show all images of selected tab
                    ourAmazingWork.randomIndexArray().forEach((element) => {
                        $($(allImages)[element]).fadeIn(1500);
                    });
                } else {
                    for (let i = 0; i < ourAmazingWork.imageToShowQuantity; i++) {
                        $($(`.${ourAmazingWork.selectedTab}`)[i]).fadeIn(1500);
                    }
                }

                //show "load More" button after 2 time clicked
                if (Number(clickCounter) !== 2) {
                    $('#loadMore').show('slow');
                }
            }, 2000);
        });
    }
};

// ourAmazingWork.loadImages(12, 'web-design');
// ourAmazingWork.loadImages(12, 'graphic-design');
// ourAmazingWork.loadImages(12, 'landing-page');
// ourAmazingWork.loadImages(12, 'wordpress');
// ourAmazingWork.showContent();
// ourAmazingWork.tabSwitch();
// ourAmazingWork.loadMore();
// launch in main_script.js