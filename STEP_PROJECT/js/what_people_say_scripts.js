const whatPeopleSay = {
    /**
     * @description using to synchronise in switching tabs of carousel function and tabSwitch function
     */
    tabCounter: 0,

    /**
     * @description change attr class of tabs and show content when clicking to right and left button, move tabs container and show hided tabs
     * @param itemWidth {number}
     * @param ItemQuantity {number}
     */
    carousel: function (itemWidth, ItemQuantity) {
        let position = 0;
        const mainContainer = document.getElementsByClassName('carousel__container')[0];
        const itemsCollection = document.getElementsByClassName('photo__wrapper__carousel');
        const leftButton = document.getElementsByClassName('carousel__left__right')[0];
        const rightButton = document.getElementsByClassName('carousel__left__right')[1];
        const tabsCollection = document.getElementsByClassName('photo__container__carousel');
        const contentCollection = document.getElementsByClassName('author__content');
        const contentContainers = $('.author__content');



        leftButton.onclick = function () {
            //deactivate all tabs
            for (let element of tabsCollection) {
                element.parentNode.classList.remove('what__people__say__active__wrapper');
                element.classList.remove('what__people__say__active__container');
            }

            //change tabCounter
            if (whatPeopleSay.tabCounter > 0) {
                whatPeopleSay.tabCounter--;
            }
            //activate choosing tab
            tabsCollection[Number(whatPeopleSay.tabCounter)].parentNode.classList.add('what__people__say__active__wrapper');
            tabsCollection[whatPeopleSay.tabCounter].classList.add('what__people__say__active__container');

            //hide all content
            for (let element of contentCollection) {
                element.style.display = 'none'
            }

            // show choosing content
            for (let i = 0; i < contentCollection.length; i++) {
                if (Number(contentCollection[i].getAttribute('data-counter')) === Number(whatPeopleSay.tabCounter)) {
                    $($(contentContainers)[i]).fadeIn(1500);
                    contentCollection[i].style.display = 'flex';
                }
            }

            //move tabs container
            if (whatPeopleSay.tabCounter === 1) {
                position += itemWidth * ItemQuantity;
                position = Math.min(position, 0);
                mainContainer.style.marginLeft = position + 'px';
                console.log(position);
                console.log(mainContainer.style.marginLeft);
            }
        };

        rightButton.onclick = function () {
            //deactivate all tabs
            for (let element of tabsCollection) {
                element.parentNode.classList.remove('what__people__say__active__wrapper');
                element.classList.remove('what__people__say__active__container');
            }

            //change tabCounter
            if (whatPeopleSay.tabCounter < 5) {
                whatPeopleSay.tabCounter++;
            }

            //activate choosing tab
            tabsCollection[whatPeopleSay.tabCounter].parentNode.classList.add('what__people__say__active__wrapper');
            tabsCollection[whatPeopleSay.tabCounter].classList.add('what__people__say__active__container');

            //hide all content
            for (let element of contentCollection) {
                element.style.display = 'none'
            }

            // show choosing content
            for (let i = 0; i < contentCollection.length; i++) {
                if (Number(contentCollection[i].getAttribute('data-counter')) === Number(whatPeopleSay.tabCounter)) {
                    $($(contentContainers)[i]).fadeIn(1500);
                    contentCollection[i].style.display = 'flex';
                }
            }

            //move tabs container
            if (whatPeopleSay.tabCounter === 4) {
                position -= itemWidth * ItemQuantity;
                position = Math.max(position, -itemWidth * (itemsCollection.length - ItemQuantity));
                mainContainer.style.marginLeft = position + 'px';
                console.log(position);
                console.log(mainContainer.style.marginLeft);
            }
        };
    },

    /**
     * @description change class of clicked tab and change global whatPeopleSay.tabCounter
     */
    tabSwitch: function () {
        const tabsCollection = document.getElementsByClassName('photo__container__carousel');

        tabsCollection[0].parentNode.classList.add('what__people__say__active__wrapper');
        tabsCollection[0].classList.add('what__people__say__active__container');

        for (let element of tabsCollection) {
            element.onclick = (event) => {

                //deactivate all tabs
                for (let element of tabsCollection) {
                    element.parentNode.classList.remove('what__people__say__active__wrapper');
                    element.classList.remove('what__people__say__active__container');
                }

                //activate clicked tabs
                event.target.parentNode.classList.add('what__people__say__active__wrapper');
                event.target.classList.add('what__people__say__active__container');
                whatPeopleSay.tabCounter = event.target.getAttribute('data-counter');
            }
        }
    },

    /**
     * @description show content of clicked tab
     */
    showContent: function () {
        const tabs = $('.photo__container__carousel');
        const contentContainers = $('.author__content');

        //show content of firs tab
        contentContainers[0].style.display = 'flex';

        $(tabs).on('click', (event) => {
            //clickedContainer = #("id of clicked tab")-content
            let clickedContainer = $(`#${$(event.target).attr('id')}-content`);

            //hide all content
            $(contentContainers).each(function () {
                $(this)[0].style.display = 'none';
            });

            //show content of clicked tabs
            clickedContainer.fadeIn(1500);
            clickedContainer[0].style.display = 'flex';
        })
    }
};

// whatPeopleSay.carousel(100, 4);
// whatPeopleSay.tabSwitch();
// whatPeopleSay.showContent();
// launch in main_script.js