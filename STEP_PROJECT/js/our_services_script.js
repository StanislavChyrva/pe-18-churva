const ourServices = {
    /**
     * @description change classList of clicked link(tab) and add rectangle (using pure JS)
     */
    tabSwitch: function () {
        const tabsCollection = document.getElementsByClassName('our__services__element__container');
        const rectangle = document.createElement('div');
        rectangle.setAttribute('class', 'rectangle');

        //activate first tab
        tabsCollection[0].classList.add('our__services__active');
        tabsCollection[0].parentNode.append(rectangle);

        //deactivate all tabs
        for (let element of tabsCollection) {
            element.onclick = (event) => {
                for (let element of tabsCollection) {
                    element.classList.remove('our__services__active');
                    rectangle.remove();
                }

                //activate clicked tab
                event.target.classList.add('our__services__active');
                event.target.parentNode.append(rectangle);
            }
        }
    },
    /**
     * @description show element with id = href of clicked tabs (using jquery)
     */
    showContent: function () {
        const tabsCollection = $('.our__services__element__container');
        const contentContainers = $('.our__service__content__container');
        contentContainers[0].style.display = 'flex';

        $(tabsCollection).on('click', (event) => {
            // clickedContainer id = attr href of clicked tabs parent (<a>)
            let clickedContainer = $($(event.target).parent().attr('href'));
            // hide all content
            $(contentContainers).each(function() {
                $(this).slideUp('fast');
            });
            event.preventDefault();
            //show clickedContainer
            clickedContainer.slideDown('fast');
        })
    }


};
// ourServices.tabSwitch();
// ourServices.showContent();
// launch in main_script.js