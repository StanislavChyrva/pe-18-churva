const breakingNew = {
    /**
     * @description create <div> with id = postContainer_(number from 1 to postQuantity) and append to mainContainer
     * @param postQuantity {number}
     */
    addNewPosts: function (postQuantity) {
        const mainContainer = document.getElementsByClassName('breaking__news__content__container')[0];

        for (let i = 1; i <= postQuantity; i++) {
            let postContainer = document.createElement('div');
            postContainer.setAttribute('class', 'breaking__news__post__content');
            postContainer.setAttribute('id', `postContainer_${i}`);

            postContainer.innerHTML = '<a href="#"><div class="breaking__news__content">\n' +
                '            <div class="image"></div>\n' +
                '            <div class="date"><span>12</span><span>Feb</span></div>\n' +
                '            <h4>Amazing Blog Post</h4>\n' +
                '            <div class="author__comment__container">\n' +
                '            <span class="author">By admin</span>\n' +
                '            <span class="comment">2 comment</span>\n' +
                '            </div></div></a>';
            mainContainer.append(postContainer);
            document.getElementsByClassName('image')[i - 1].style.backgroundImage = `url("img/breakingNews/breaking-news-${i}.png")`;

        }

    }

};

// breakingNew.addNewPosts(8);
// launch in main_script.js