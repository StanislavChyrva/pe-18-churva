document.addEventListener('DOMContentLoaded', onLoadReady);

function onLoadReady () {
    breakingNew.addNewPosts(8);

    ourServices.tabSwitch();
    ourServices.showContent();

    ourAmazingWork.loadImages(12, 'web-design');
    ourAmazingWork.loadImages(12, 'graphic-design');
    ourAmazingWork.loadImages(12, 'landing-page');
    ourAmazingWork.loadImages(12, 'wordpress');
    ourAmazingWork.showContent();
    ourAmazingWork.tabSwitch();
    ourAmazingWork.loadMore();

    whatPeopleSay.carousel(100, 4);
    whatPeopleSay.tabSwitch();
    whatPeopleSay.showContent();
}